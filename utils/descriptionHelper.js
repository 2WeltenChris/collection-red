const getConflictDescription = (node) => {
  let conflDescr = ''
  if (node.collectionType === 'Array' && node.action === 'insert') return 'collection will be created'
  switch (node.onConflict) {
    case 'exception':
      conflDescr = 'throw exception'
      break
    case 'output':
      conflDescr = 'send msg to error output'
      break
    case 'discard':
      conflDescr = 'flow will be stopped'
      break
    case 'default':
      conflDescr = 'return value set in bottom input field'
      break
    case 'keep':
      if (node.action === 'set') conflDescr = 'value will not be set'
      else if (node.action === 'insert') conflDescr = 'existing value will not be changed'
      break
    case 'overwrite':
      if (node.action === 'set' && node.collectionType !== "Array") conflDescr = 'value will be inserted'
      else if (node.action === 'set' && node.collectionType === "Array") conflDescr = 'value will be overwritten'
      else if (node.action === 'insert') conflDescr = 'existing value will be changed'
      break
  }
  return conflDescr
}

const createMapLikeActionDescr = (action) => {
  switch (action) {
    case 'insert':
      return 'Creates a new key value pair'
    case 'set':
      return 'Changes the value of an existing key'
    case 'get value':
      return 'Returns the value for the given key'
    case 'get values':
      return 'Returns ALL values'
    case 'has key':
      return 'Returns true if exists or false if not'
    case 'get keys':
      return 'Returns all keys'
    case 'get size':
      return 'Returns the size of the Object'
    case 'get entries':
      return 'Returns all key value pairs as array'
    case 'delete key':
      return 'Deletes the entry corresponding to the key'
    case 'clear':
      return 'Deletes all properties'
  }
  return ''
}

const createArrayActionDescr = (action) => {
  switch (action) {
    case 'insert':
      return 'Appends the value at this position'
    case 'set':
      return 'Overwrite the value at this position'
    case 'get value':
      return 'Returns the value of the given index'
    case 'get size':
      return 'Returns the array length'
    case 'delete index':
      return 'Removes the value at this position'
    case 'clear':
      return 'Removes all values from the array'
  }
  return ''
}

const createSetActionDescr = (action) => {
  switch (action) {
    case 'add':
      return 'Adds value to the set if not existing'
    case 'has value':
      return 'Returns true if the value exists else false'
    case 'get values':
      return 'Returns ALL values of the set'
    case 'get size':
      return 'Returns the size of the set'
    case 'delete value':
      return 'Removes the stated value from the set'
    case 'clear':
      return 'Removes ALL values from the set'
  }
  return ''
}

const createOnConflictMapLike = (node) => {
  let descr = 'if key '
  if (['get keys', 'get values', 'get size', 'get entries', 'clear'].includes(node.action)) descr = 'if collection '
  if (node.action === 'insert') descr += 'exists then '
  else descr += 'does not exist then '
  descr += getConflictDescription(node)
  return descr
}

const createOnConflictArray = (node) => {
  let reason = 'if index exists then '
  switch (node.action) {
    case 'insert':
      reason = 'if collection does not exists then '
      if (node.onConflict === 'keep') return reason + 'ignore and move on'
      else if (node.onConflict === 'overwrite') return reason + 'create and insert value'
      else return reason + getConflictDescription(node)
    case 'set':
      if (node.onConflict === 'keep') return reason + 'ignore and move on'
      else if (node.onConflict === 'overwrite') return reason + getConflictDescription(node)
      else return reason + getConflictDescription(node)
    case 'overwrite':
      if (node.action === 'set') return reason + 'overwrite it or create collection if needed'
      else return 'if index does not exist then ' + getConflictDescription(node)
    default:
      return 'if index does not exist then ' + getConflictDescription(node)
  }
}

const createOnConflictSet = (node) => {
  if (node.onConflict === 'keep') {
    if (node.action === 'add') return 'if value exists then ignore and move on'
    if (node.action === 'delete value') return 'if value does not exists then ignore and move on'
  }
  switch (node.action) {
    case 'has value':
    case 'get values':
    case 'get size': 
    case 'clear': 
      return 'if collection does not exists then throw exception'
    default:
      return 'if value does not exists then ' + getConflictDescription(node)
  }
}

const createOnConflictDescription = (node) => {
  let descr = ''
  switch (node.collectionType) {
    case 'Object':
      descr = createOnConflictMapLike(node)
      break
    case 'Map':
      descr = createOnConflictMapLike(node)
      break
    case 'Set':
      descr = createOnConflictSet(node)
      break
    case 'Array':
      descr = createOnConflictArray(node)
      break
  }
  return descr
}

const createActionDescription = (node) => {
  let descr = ''
  switch (node.collectionType) {
    case 'Object':
      descr = createMapLikeActionDescr(node.action)
      break
    case 'Map':
      descr = createMapLikeActionDescr(node.action)
      break
    case 'Set':
      descr = createSetActionDescr(node.action)
      break
    case 'Array':
      descr = createArrayActionDescr(node.action)
      break
  }
  return descr
}

module.exports = {
  createOnConflictDescription,
  createActionDescription
}
