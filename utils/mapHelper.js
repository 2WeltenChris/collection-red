const conflictHelper = require('./conflictHelper')

const doAction = (node, msg, config, c) => {
  const valueExists = (typeof c.collection.get(c.keyOrIndex) !== 'undefined')

  const stopAction = conflictHelper.handleConflict(node, msg, config, c, valueExists)
  if (config.action !== 'has key' && stopAction) return null

  switch (config.action) {
    case 'insert':
      c.collection.set(c.keyOrIndex, c.value)
      break
    case 'set':
      c.collection.set(c.keyOrIndex, c.value)
      break
    case 'get value':
      msg.payload = c.collection.get(c.keyOrIndex)
      break
    case 'get values':
      msg.payload = [...c.collection.values()] // iterator would return undefined by node.send() -> convert to array
      break
    case 'has key':
      msg.payload = c.collection.has(c.keyOrIndex)
      break
    case 'get keys':
      msg.payload = [...c.collection.keys()] // iterator would return undefined by node.send() -> convert to array
      break
    case 'get size':
      msg.payload = c.collection.size
      break
    case 'get entries':
      msg.payload = [...c.collection.entries()] // iterator would return undefined by node.send() -> convert to array
      break
    case 'delete key':
      c.collection.delete(c.keyOrIndex)
      break
    case 'clear':
      c.collection.clear()
      break
    default:
      throw new Error('No valid action set')
  }
  node.send(msg)
}

module.exports = {
  doAction
}
