const conflictHelper = require('./conflictHelper')

const doAction = (node, msg, config, c) => {
  const valueExists = (typeof c.collection[c.keyOrIndex] !== 'undefined' && c.collection[c.keyOrIndex] !== null)
  const stopAction = conflictHelper.handleConflict(node, msg, config, c, valueExists)
  if (stopAction) return null

  switch (config.action) {
    case 'insert':
      c.collection[c.keyOrIndex] = c.value
      break
    case 'set':
      c.collection[c.keyOrIndex] = c.value
      break
    case 'get value':
      msg.payload = c.collection[c.keyOrIndex]
      break
    case 'get values':
      msg.payload = Object.values(c.collection)
      break
    case 'get keys':
      msg.payload = Object.keys(c.collection)
      break
    case 'get size':
      msg.payload = Object.keys(c.collection).length
      break
    case 'get entries':
      msg.payload = Object.entries(c.collection)
      break
    case 'delete key':
      delete c.collection[c.keyOrIndex]
      break
    case 'clear':
      Object.keys(c.collection).forEach(k => delete c.collection[k]) // set to {} wouldn't work
      break
    default:
      throw new Error('No valid action set')
  }
  node.send(msg)
}

module.exports = {
  doAction
}
