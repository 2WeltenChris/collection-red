const conflictHelper = require('./conflictHelper')

const doAction = (node, msg, config, c) => {
  c.keyOrIndex = c.value
  // Set has only on those two actions a conflict
  if (config.action === 'add' || config.action === 'delete value') {
    const valueExists = c.collection.has(c.value)
    const stopAction = conflictHelper.handleConflict(node, msg, config, c, valueExists)
    if (stopAction) return null
  }

  switch (config.action) {
    case 'add':
      c.collection.add(c.value)
      break
    case 'has value':
      msg.payload = c.collection.has(c.value)
      break
    case 'get values':
      msg.payload = [...c.collection.values()]
      break
    case 'get size':
      msg.payload = c.collection.size
      break
    case 'delete value':
      c.collection.delete(c.value)
      break
    case 'clear':
      c.collection.clear()
      break
    default:
      throw new Error('No valid action set for "Set"')
  }
  node.send(msg)
}

module.exports = {
  doAction
}
