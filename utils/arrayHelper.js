const conflictHelper = require('./conflictHelper')

const doAction = (node, msg, config, c) => {
  // Handle Array special cases 
  if (config.keyInputType === 'first') c.keyOrIndex = 0
  if (config.keyInputType === 'last') {
    c.keyOrIndex = 0
    if (config.action === 'insert') c.keyOrIndex = c.collection.length
    else if (c.collection.length) c.keyOrIndex = c.collection.length - 1
  }
  if (isNaN(c.keyOrIndex)) throw new Error('No valid index number set. Index is type of ' + typeof c.keyOrIndex)

  const valueExists = (typeof c.collection[c.keyOrIndex] !== 'undefined' && c.collection[c.keyOrIndex] !== null)
  const stopAction = conflictHelper.handleConflict(node, msg, config, c, valueExists)
  if (stopAction) return null

  switch (config.action) {
    case 'insert':
      if (typeof c.collection[c.keyOrIndex] !== 'undefined') c.collection.splice(c.keyOrIndex, 0, c.value) // put between existing indexes
      else c.collection[c.keyOrIndex] = c.value // extend array
      break
    case 'set':
      c.collection[c.keyOrIndex] = c.value
      break
    case 'get value':
      msg.payload = c.collection[c.keyOrIndex]
      break
    case 'get size':
      msg.payload = c.collection.length
      break
    case 'delete index':
      c.collection.splice(c.keyOrIndex, 1)
      break
    case 'clear':
      c.collection.length = 0
      break
    default:
      throw new Error('No valid action set')
  }
  node.send(msg)
}

module.exports = {
  doAction
}
