const handleConflict = (node, msg, config, c, valueExists) => {
  let hasConflict = ((valueExists && config.action === 'insert') || (!valueExists && config.action !== 'insert'))

  if (config.collectionType === 'Set' && config.action === 'add') {
    hasConflict = valueExists
  }
  
  if (config.collectionType === 'Array') {
    const setOnExists = (valueExists && config.action === 'set')
    const deleteNotExisting = (!valueExists && config.action === 'delete index')
    const getOutsideLength = (c.collection.length <= c.keyOrIndex && config.action === 'get value')
    hasConflict = (setOnExists || deleteNotExisting || getOutsideLength)
  }

  // those actions should not use this conflict management
  if (['has key', 'has value', 'get values', 'get keys', 'get size', 'get entries', 'clear'].includes(config.action)) {
    hasConflict = false
  }

  if (hasConflict) {
    let conflictType = 'Key'
    let reason = (config.action === 'insert' || config.action === 'add') ? 'already exists' : 'does not exist'
    if (config.collectionType === 'Array') {
      conflictType = 'Index'
      if (config.action === 'set') reason = 'exists'
    } else if (config.collectionType === 'Set') {
      conflictType = 'Value'
    }

    let error = `${conflictType} ${c.keyOrIndex} in collection ${config.collectionInputType}.${config.collection} ${reason}`

    switch (config.onConflict) {
      case 'exception':
        throw new Error(error)
      case 'output':
        msg.error = { message: 'Error:' + error }
        node.send([null, msg])
        return true
      case 'discard':
        if (config.warnOnConflict) node.warn('Warning: ' + error)
        return true
      case 'keep':
        if (config.warnOnConflict) node.warn('Warning: ' + error)
        node.send(msg)
        return true
      case 'default':
        if (config.warnOnConflict) node.warn('Warning: ' + error)
        if (config.valueInputType === '$null') {
          msg.payload = null
        } else {
          msg.payload = c.value
        }
        node.send(msg)
        return true
      case 'overwrite':
        if (config.action === 'set') error += ' but will be created'
        if (config.warnOnConflict) node.warn('Warning: ' + error)
        break
      default:
        throw new Error('No valid onConflict handling set!')
    }
  }
  return false
}

module.exports = {
  handleConflict
}
