const getEmptyCollection = (collectionType) => {
  switch (collectionType) {
    case 'Object':
      return {}
    case 'Map':
      return new Map()
    case 'Array':
      return []
    case 'Set':
      return new Set()
    default:
      throw new Error("Invalid collection type. Can't create collection!")
  }
}

const createNewCollection = (RED, node, msg, config, c) => {
  const collection = getEmptyCollection(config.collectionType)
  let target = config.collection
  // if (c.keyOrIndex) target = target + '.' + c.keyOrIndex
  // if (config.collectionType === 'Array') target = config.collection

  // We must set this pointer again
  switch (config.collectionInputType) {
    case 'msg':
      RED.util.setMessageProperty(msg, target, collection)
      break
    case 'flow':
      // just in case: Node-RED change node handles it that way: RED.util.evaluateNodeProperty(rule.to, rule.tot, node, msg, (err,value) => { if (err) { done(undefined,undefined);} else {done(undefined,value); } })
      node.context().flow.set(target, collection)
      break
    case 'global':
      node.context().global.set(target, collection)
      // c.global.set(target, collection)
      break
    default:
      throw new Error('Invalid collection input type!')
  }
  c.collection = collection
}

// Check if not null, typeof and is no Array... then we assume it is a "Object" {}
const objectIsObject = (collection) => {
  return (!!collection && typeof collection === 'object' && Array.isArray(collection) === false)
}

const checkCollectionType = (type, collection) => {
  // instanceof Set/Map seems not to work. Maybe proxy variable through Node-Red?
  const e = 'collection is no ' + type
  switch (type) {
    case 'Array':
      // if (!(collection instanceof Array)) throw new Error(e)
      if (!Array.isArray(collection)) throw new Error(e)
      break
    case 'Object':
      if (!objectIsObject(collection)) throw new Error(e)
      break
    case 'Map':
      if (collection.constructor.name !== 'Map') throw new Error(e)
      break
    case 'Set':
      if (collection.constructor.name !== 'Set') throw new Error(e)
      break
    default:
      throw new Error('Invalid collection type set')
  }
}

module.exports = {
  createNewCollection,
  checkCollectionType
}
