# Changelog

## Version 0.0.5 (17th of November 2021)

- changed Array get start / end to first / last
- Array delete index gets conflict options
- fixed auto naming 

## Version 0.0.4 (17th of November 2021)

- working with latest node-red
- fixed Array detection
- fixed conflict options
- fixed description
- updated to latest svelte-integration-red

## Version 0.0.3 (26th of July 2021)

- added description
- readme fix