# Collection-RED

Collection-RED is a node which helps you to manage your context objects within a flow more easily.

![context](https://gitlab.com/2WeltenChris/collection-red/raw/master/examples/context.png?raw=true "context")

## Usage

Just set the location of your collection and the type (Object, Array, Map, Set). Then you can choose from a variation of possible actions designed for that collection type.

There is a description for the selected action as well as what can be a possible conflict.

For example: In this picture below we want to insert the payload value at the end of an array located at flow.collection. This is equal to using the function node and use flow.collection.push(msg.payload).

![Array-Example](https://gitlab.com/2WeltenChris/collection-red/raw/master/examples/Array-example.png?raw=true "Array-Example")

## Conflict handling

As your action may create a conflict, (e.g. insert an object key, which already exists) you have to choose how this conflict should be handled.

![conflict](https://gitlab.com/2WeltenChris/collection-red/raw/master/examples/conflict.png?raw=true "conflict")

- If that key should not exist at that moment throw an error and stop the flow
- Create a second node output, to switch the flow direction
- Discard the message will stop the flow without throwing an error
- Keep the value or
- Overwrite the value (like action 'set')

## Examples

There is an example flow which can be imported which shows for all four object types and all actions an example.

This node can also be tested by using gitpod.

## Developers

This node was created with [Svelte-Integration-RED](https://gitlab.com/2WeltenChris/svelte-integration-red). Therefore changes should be made within the collection-red.svelte file and then use the command ```sir```.
