const collectionHelper = require('./utils/collectionHelper')
const conflictHelper = require('./utils/conflictHelper')
const arrayHelper = require('./utils/arrayHelper')
const objectHelper = require('./utils/objectHelper')
const mapHelper = require('./utils/mapHelper')
const setHelper = require('./utils/setHelper')

module.exports = function (RED) {
  function collectionRed (config) {
    RED.nodes.createNode(this, config)
    const node = this
    node.on('input', function (msg) {
      const c = {
        // flow: this.context().flow,
        // global: this.context().global,
        collection: RED.util.evaluateNodeProperty(config.collection, config.collectionInputType, this, msg),
        keyOrIndex: RED.util.evaluateNodeProperty(config.keyOrIndex, config.keyInputType, this, msg),
        value: RED.util.evaluateNodeProperty(config.value, config.valueInputType, this, msg)
      }

      // downward compatible
      if (c.collectionType === 'Array' && c.keyOrIndex === 'start') c.keyOrIndex = 'first'
      if (c.collectionType === 'Array' && c.keyOrIndex === 'end') c.keyOrIndex = 'last'
      
      // check collection exists or shall be created
      if (c.collection === null || typeof c.collection === 'undefined') {
        if (!config.createCollection) {
          // if (config.collectionType === 'Array' && (config.action === 'insert' || config.action === 'set')) {
          //   const stopFlow = conflictHelper.handleConflict(node, msg, config, c, (config.action === 'insert'))
          //   if (stopFlow) return null
          // }
          return node.error('collection not found and should not be created!')
        } else {
          collectionHelper.createNewCollection(RED, node, msg, config, c)
          c.collection = RED.util.evaluateNodeProperty(config.collection, config.collectionInputType, this, msg)
        }
      }
      try {
        collectionHelper.checkCollectionType(config.collectionType, c.collection)
        switch (config.collectionType) {
          case 'Object':
            objectHelper.doAction(node, msg, config, c)
            break
          case 'Map':
            mapHelper.doAction(node, msg, config, c)
            break
          case 'Array':
            arrayHelper.doAction(node, msg, config, c)
            break
          case 'Set':
            setHelper.doAction(node, msg, config, c)
            break
        }
      } catch (e) {
        return node.error(e, msg)
      }
    })
  }
  RED.nodes.registerType('collection', collectionRed)
}
